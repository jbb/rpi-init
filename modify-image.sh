#!/usr/bin/env bash

set -e

# This script works for Raspberry Pi images, but you will need to modify the root partition number for other boards

if [ $# -le 1 ]; then
	echo "Usage: ./modify-image.sh <image> <rpi-init.py args...>"
	exit 1
fi

function unmount_loop() {
	sudo losetup -d /dev/loop1
}

sudo losetup -P /dev/loop1 "$1"
trap unmount_loop EXIT

./rpi-init.py /dev/loop1p2 "${@:2}"

bmaptool create "$1" -o "$1.bmap"
