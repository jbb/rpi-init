#!/usr/bin/env python3

import subprocess
import sys
from pathlib import Path
from subprocess import CalledProcessError

from typing import List

MOUNTED_PATHS: List[Path] = []


def mount(source: Path, dest: Path, bind=False,
          newinst=False, ptype=None):
    extra_args: List[str] = []
    if bind:
        extra_args.append("--bind")
    if newinst:
        extra_args += ["-o", "newinstance"]
    if ptype:
        extra_args += ["-t", ptype]

    mountcmd = ["sudo", "mount", str(source), str(dest)] + extra_args
    print("Mounting", dest)
    subprocess.check_call(mountcmd)

    MOUNTED_PATHS.append(dest)


def mount_virt_kernel_fs(root_dir: Path, source: Path, **args):
    mount(Path("/") / source, root_dir / source, bind=True, **args)


def unmount(dest: Path):
    print("Unmounting", dest)
    # Best effort unmounting
    subprocess.call(["sudo", "umount", str(dest)])


def unmount_all():
    for path in reversed(MOUNTED_PATHS):
        unmount(path)


def run_in_chroot(root_dir: Path, command: List[str], **args):
    print("chroot: Running", command)
    subprocess.check_call(["sudo", "chroot", str(root_dir)] + command, **args)


def run_in_chroot_shell(root_dir: Path, command: str):
    run_in_chroot(root_dir, ["bash", "-c", command])


def apt(root_dir: Path, command: List[str]):
    run_in_chroot(root_dir, ["apt-get"] + command, env={
            "DEBIAN_FRONTEND": "noninteractive"
        })


def copy_file(source: Path, dest: Path):
    subprocess.check_call(["sudo", "cp", str(source), str(dest)])


def chown_in_chroot(root_dir: Path, user: str, group: str, file: Path):
    run_in_chroot(root_dir, ["chown", "-R", f"{user}:{group}", str(file)])


def main(root_part: Path, hostname: str, user: str, user_password_hash: str,
         root_password_hash: str, ssh_pub_path: Path):
    root_dir = Path("/mnt")
    mount(root_part, root_dir)
    mount_virt_kernel_fs(root_dir, Path("dev"))
    mount(Path("devpts"), root_dir / "dev/pts", newinst=True, ptype="devpts")
    mount_virt_kernel_fs(root_dir, Path("sys"))
    mount_virt_kernel_fs(root_dir, Path("proc"))
    mount_virt_kernel_fs(root_dir, Path("run"))
    copy_file(Path("/etc/resolv.conf"), root_dir / "etc" / "resolv.conf")

    # Install sudo
    apt(root_dir, ["update"])
    apt(root_dir, ["install", "sudo", "-y"])

    # Install python3 (needed for ansible)
    apt(root_dir, ["install", "python3", "-y"])

    # Set hostname
    run_in_chroot_shell(root_dir, f"echo {hostname} > /etc/hostname")
    run_in_chroot_shell(root_dir, f"echo '127.0.0.1 {hostname}' >> /etc/hosts")

    # Add user and group
    try:
        run_in_chroot(root_dir, ["useradd", "--create-home", "-s", "/bin/bash",
                                 "-G", "sudo", user])
    except CalledProcessError as e:
        if e.returncode == 9:
            print("Username already exists, ignoring")

    run_in_chroot(root_dir, ["usermod", "-p", password_hash, user])
    run_in_chroot(root_dir, ["usermod", "-p", root_password_hash, "root"])

    # Remove possible predefined users
    if user != "pi":
        try:
            run_in_chroot(root_dir, ["deluser", "pi"])
        except CalledProcessError as e:
            if e.returncode == 2:
                print("User pi did not exist, ignoring")
    else:
        print("Keeping default pi user")

    # Remove pi user configuration utility, we already created a user.
    # If we keep it, it prints annoyig messages to the ssh session.
    run_in_chroot(root_dir, ["dpkg", "--purge", "userconf-pi"])

    # Set up ssh
    apt(root_dir, ["install", "openssh-server", "-y"])
    run_in_chroot(root_dir, ["systemctl", "enable", "ssh"])
    authorized_keys_path = Path("/home") / user / ".ssh"
    run_in_chroot(root_dir, ["mkdir", "-p", str(authorized_keys_path)])
    copy_file(ssh_pub_path,
              root_dir / "home" / user / ".ssh" / "authorized_keys")
    chown_in_chroot(root_dir, user, user,
                    Path("home") / user)

    # Generate locales
    apt(root_dir, ["install", "locales", "-y"])
    run_in_chroot(root_dir, ["locale-gen"])

    unmount_all()


if __name__ == "__main__":
    if len(sys.argv) < 7:
        print("Usage: ./rpi-init.py /dev/root_part hostname "
              + "username '<password hash>' '<root password hash>' "
              + "~/.ssh/id_ed25519.pub")
        exit(1)

    partition_path = Path(sys.argv[1])
    hostname = sys.argv[2]
    user = sys.argv[3]
    password_hash = sys.argv[4]
    root_password_hash = sys.argv[5]
    ssh_key = Path(sys.argv[6])
    try:
        main(partition_path, hostname, user, password_hash,
             root_password_hash, ssh_key)
    except Exception as e:
        print(e)
        unmount_all()
