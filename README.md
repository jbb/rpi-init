# rpi-init

Install just enough in a debian based operating system image to continue with ansible.

This script automates the following tasks:
 - Install sudo (for become)
 - Install python (for ansible)
 - Set a hostname, so the device can be found easily on the network
 - create a new user with sudo permissions
 - Set a root password
 - Delete the default pi user (if running on Raspberry Pi OS)
 - Install an ssh server
 - Remove first start setup userconf-pi if it exists
 - Copy the ssh public key

## Usage

To be compatible with different kinds of images, rpi-init does not hard code the partition layout.

That means, you got to tell the script were the root partition is.

First, loop-mount the image (only make the partitions available, but don't mount them):
```sh
losetup -P /dev/loop0 image.img
```

You can use `fdisk -l` to find out were the root partition is. For Raspberry Pi OS, that is `/dev/loop0p2`.

The password hashes can be created using `mkpasswd`.

```sh
./rpi-init.py /dev/<root_part> <hostname> <username> '<password hash>' '<root password hash>' ~/.ssh/id_ed25519.pub
```
